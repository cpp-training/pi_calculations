#include <gtest/gtest.h>

#include <iostream>
#include <vector>
#include <Eigen/Dense>

using namespace Eigen;


TEST(MatrixTest, Determinant)
{
    Eigen::MatrixXd A(4, 4);
    A << 1, 2, 0, 2,
         3, 4, 0, -2,
         0, 0, 5, 1,
         2, -2, 1, 3;

    double expected_determinant = A.determinant();

    std::cout << "A.determinant() = " << A.determinant() << std::endl;


    ASSERT_FLOAT_EQ(A.determinant(), expected_determinant);
}
