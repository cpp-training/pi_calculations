#include <iostream>

#include <gtest/gtest.h>

#include <pi_calculator/utilities/cout_helpers.h>

TEST(DUMMY_TEST, TEST_dummy)
{
    std::vector<int> actual_vec{ 0, 1, 2, 3 };

    std::cout << "actual_vec: " << actual_vec << std::endl;

    EXPECT_EQ(actual_vec, std::vector<int>({ 0, 1, 2, 3 }));
}
