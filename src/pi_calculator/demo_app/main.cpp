#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits>
#include <string>
#include <thread>
#include <vector>

#include <pi_calculator/utilities/cout_helpers.h>

#include <pi_calculator/core/catalog.h>
#include <pi_calculator/core/madhava.h>
#include <pi_calculator/core/partial_adder.h>
#include <pi_calculator/core/pi_calculator.h>

using timestamp_t = std::chrono::steady_clock::time_point;

int main()
{
    std::vector<std::string> vec = { "Hello", "from", "GCC", __VERSION__, "!" };
    std::cout << vec << std::endl;

    // TODO parse a program option to select pi approximation

    // Display available approximators
    std::cout << "Available approximators follow" << std::endl;
    for (auto const& elem : pi_approximators)
    {
        std::cout << elem.first << std::endl;
    }

    auto full_ = std::setprecision(std::numeric_limits<int>::digits);

    double pi_mad_1k = pi_madhava(1000);
    double pi_mad_10k = pi_madhava(10000);
    double pi_mad_100k = pi_madhava(100000);
    double pi_mad_500k = pi_madhava(500000);

    std::cout << "pi_madhava - 1000 termes : " << full_ << pi_mad_1k << std::endl;
    std::cout << "pi_madhava - 10 000 termes : " << full_ << pi_mad_10k << std::endl;
    std::cout << "pi_madhava - 100 000 termes : " << full_ << pi_mad_100k << std::endl;
    std::cout << "pi_madhava - 500 000 termes : " << full_ << pi_mad_500k << std::endl;

    double delta_mad500k = std::abs(M_PI - pi_mad_500k);
    std::cout << "delta_mad500k : " << full_ << delta_mad500k << std::endl;

    // 50 000 000
    double pi_mad_50M = pi_madhava(50000000);
    double delta_mad50M = std::abs(M_PI - pi_mad_50M);
    std::cout << "delta_mad50M : " << full_ << delta_mad50M << std::endl;

    // 1 000 000 000
    //    timestamp_t l_start = std::chrono::steady_clock::now();

    //    PiSeqCalculator madhava;
    //    double pi_mad_G = madhava.compute_pi(1e9);

    //    double delta_madG = std::abs(M_PI - pi_mad_G);
    //    std::cout << "delta_madhava 1e9 terms: " << full_ << delta_madG << std::endl;

    //    timestamp_t l_stop = std::chrono::steady_clock::now();
    //    int64_t elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(l_stop - l_start).count();

    //    std::cout << "Time required to compute 1e9 terms" << std::endl;
    //    std::cout << "global time : " << elapsed_ms << " ms " << std::endl;

    std::cout << "\n" << std::endl;

 
}
