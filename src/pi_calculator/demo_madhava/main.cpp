#include <chrono>
#include <cmath>
#include <iomanip>
#include <iostream>
#include <limits>
#include <string>
#include <thread>
#include <vector>

#include <pi_calculator/utilities/cout_helpers.h>

#include <pi_calculator/core/catalog.h>
#include <pi_calculator/core/madhava.h>
#include <pi_calculator/core/partial_adder.h>
#include <pi_calculator/core/pi_calculator.h>

using timestamp_t = std::chrono::steady_clock::time_point;

int main()
{
    std::vector<std::string> vec = { "Hello", "from", "GCC", __VERSION__, "!" };
    std::cout << vec << std::endl;

    uint64_t nelem = 1e9;

    timestamp_t l_start_seq = std::chrono::steady_clock::now();

    PiSeqCalculator madhava;
    double pi_seq_Gterms = madhava.compute_pi(nelem);
    std::cout << "Sequential delta = " << std::abs(M_PI - pi_seq_Gterms) << std::endl;

    timestamp_t l_stop_seq = std::chrono::steady_clock::now();
    int64_t elapsed_seq_ms = std::chrono::duration_cast<std::chrono::milliseconds>(l_stop_seq - l_start_seq).count();

    std::cout << "Time required to compute 1e9 terms" << std::endl;
    std::cout << "1 thread cpu time : " << elapsed_seq_ms << " ms " << std::endl;

    timestamp_t l_start = std::chrono::steady_clock::now();

    double res1 = 0.;
    double res2 = 0.;
    double res3 = 0.;
    double res4 = 0.;

    int nb_threads = 2;

    PartialAdder adder1{ 0, nelem/2, &res1, &madhava_partial };
    PartialAdder adder2{ nelem/2, nelem, &res2, &madhava_partial };

    std::thread t1{ adder1 };
    std::thread t2{ adder2 };

    // wait for the threads to terminate
    t1.join();
    t2.join();

    timestamp_t l_stop = std::chrono::steady_clock::now();
    int64_t elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(l_stop - l_start).count();

    std::cout << "Time required to compute 1e9 terms" << std::endl;
    std::cout << std::to_string(nb_threads) << " threads cpu time : " << elapsed_ms << " ms " << std::endl;

    double pi_threaded_Gterms = res1 + res2 + res3 + res4;
    std::cout << "Threaded delta = " << std::abs(M_PI - pi_threaded_Gterms) << std::endl;
}
