#include "partial_adder.h"

PartialAdder::PartialAdder(uint64_t a_first,
                           uint64_t a_last,
                           double* a_partial,
                           std::function<void(uint64_t, uint64_t, double*)> a_approx)
  : m_first_index(a_first)
  , m_last_index(a_last)
  , m_partial_sum(a_partial)
  , m_pi_approximator(a_approx)
{
}

void PartialAdder::operator()()
{
    m_pi_approximator(m_first_index, m_last_index, m_partial_sum);
}
