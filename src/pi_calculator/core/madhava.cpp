#include <cmath>
#include <iostream>

#include "madhava.h"

double pi_madhava(uint64_t sum_size)
{
    double pi_o_4 = 0.;

    uint64_t ielem = 0;
    double sign = 1.;
    for (ielem = 0; ielem < sum_size; ++ielem)
    {
        uint64_t denom = 2 * ielem + 1;

        // ielem is even or odd ?
        sign = ielem / 2. == std::floor(ielem / 2.) ? 1. : -1.;

        pi_o_4 = pi_o_4 + sign * 1. / static_cast<double>(denom);

        // std::cout << sign * 1. / static_cast<double>(denom) << std::endl;
    }

    return 4. * pi_o_4;
}

void madhava_partial(uint64_t a_first, uint64_t a_last, double* partial)
{
    *partial = 0.;

    uint64_t ielem = 0;
    double sign = 1.;
    for (ielem = a_first; ielem < a_last; ++ielem)
    {
        uint64_t denom = 2 * ielem + 1;

        // ielem is even or odd ?
        sign = ielem / 2. == std::floor(ielem / 2.) ? 1. : -1.;

        *partial += sign * 1. / static_cast<double>(denom);

        // std::cout << sign * 1. / static_cast<double>(denom) << std::endl;
    }

    *partial *= 4.;
}
