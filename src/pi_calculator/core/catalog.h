#pragma once

#include <cstdint>
#include <map>
#include <string>

enum class PiApproximator : uint8_t
{
    VIETE = 0,
    WALLIS,
    MADHAVA,
    NILAKANTHA
};

static const std::map<std::string, PiApproximator> pi_approximators = { { "viete", PiApproximator::VIETE },
                                                                        { "wallis", PiApproximator::WALLIS },
                                                                        { "madhava", PiApproximator::MADHAVA },
                                                                        { "nilakantha", PiApproximator::NILAKANTHA } };
