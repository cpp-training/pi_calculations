#pragma once

#include <cstdint>

double pi_madhava(uint64_t sum_size);

void madhava_partial(uint64_t a_first, uint64_t a_last, double* partial);
