#pragma once

#include <cstdint>
#include <functional>
#include <memory>

class PartialAdder
{
public:
    PartialAdder() = delete;

    PartialAdder(uint64_t a_first,
                 uint64_t a_last,
                 double* a_partial,
                 std::function<void(uint64_t, uint64_t, double*)> a_approx);

    PartialAdder(const PartialAdder&) = default;
    PartialAdder& operator=(const PartialAdder&) = default;

    PartialAdder(PartialAdder&&) = default;
    PartialAdder& operator=(PartialAdder&&) = default;

    ~PartialAdder() = default;

    // no argument required, if this object is properly built
    void operator()();

private:
    uint64_t m_first_index;
    uint64_t m_last_index;

    double* m_partial_sum;

    std::function<void(uint64_t, uint64_t, double* result)> m_pi_approximator;
};
