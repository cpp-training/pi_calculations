#pragma once

#include <cstdint>
#include <functional>

class PiSeqCalculator
{
public:
    PiSeqCalculator();

    PiSeqCalculator(const PiSeqCalculator&) = default;
    PiSeqCalculator& operator=(const PiSeqCalculator&) = default;

    PiSeqCalculator(PiSeqCalculator&&) = delete;
    PiSeqCalculator& operator=(PiSeqCalculator&&) = delete;

    ~PiSeqCalculator() = default;

    double compute_pi(uint64_t sum_size);

private:
    std::function<double(uint64_t)> m_pi_approximator;
};
