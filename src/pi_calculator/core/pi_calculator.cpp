#include <pi_calculator/core/madhava.h>

#include "pi_calculator.h"

PiSeqCalculator::PiSeqCalculator()
{
    m_pi_approximator = &pi_madhava;
}

double PiSeqCalculator::compute_pi(uint64_t sum_size)
{

    return m_pi_approximator(sum_size);
}
